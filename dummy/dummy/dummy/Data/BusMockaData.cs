﻿using dummy.Models;
using System.Collections.Generic;

namespace dummy.Data
{
    public class BusMockaData
    {
        public static BusMockaData Current { get; } = new BusMockaData();

        public List<BusDTO> Buses { get; set; }

        BusMockaData()
        {
            Buses = new List<BusDTO>()
            {
                new BusDTO()
                {
                    Id=1,
                    Make = "Mercedes",
                    Plate = "34QQ52"
                },
                 new BusDTO()
                {
                    Id=2,
                    Make = "BWM",
                    Plate = "90CCC90"
                },
                  new BusDTO()
                {
                    Id=3,
                    Make = "Volvo",
                    Plate = "20AA87"
                },
                   new BusDTO()
                {
                    Id=4,
                    Make = "Tofaş",
                    Plate = "58SS34"
                }
            };
        }
    }
}
