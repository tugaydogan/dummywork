﻿using dummy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dummy.Data
{
    public class PassengerMockData
    {
        public static PassengerMockData Current { get; } = new PassengerMockData();

        public List<PassengerDTO> Passengers { get; set; }

        PassengerMockData()
        {
            Passengers = new List<PassengerDTO>();
        }
    }
}
