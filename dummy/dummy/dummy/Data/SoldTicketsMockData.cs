﻿using dummy.Models;
using System.Collections.Generic;

namespace dummy.Data
{
    public class SoldTicketsMockData
    {
        public static SoldTicketsMockData Current { get; } = new SoldTicketsMockData();

        public List<TicketDTO> Tickets { get; set; }

        SoldTicketsMockData()
        {
            Tickets = new List<TicketDTO>();
        }
    }
}
