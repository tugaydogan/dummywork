﻿using dummy.Models;
using System.Collections.Generic;

namespace dummy.Data
{
    public class DriverMockData
    {
        public static DriverMockData Current { get; } = new DriverMockData();

        public List<DriverDTO> Drivers { get; set; }

        public DriverMockData()
        {
            Drivers = new List<DriverDTO>()
            {


                new DriverDTO()
                {
                    Id = 1,
                    Age = 28,
                    Gender = "Erkek",
                    Name = "Fahri",
                    IdentityNumber = "12345678901",
                    LastName = "Tut",
                    LicenseType = "Yüksek"
                },
                new DriverDTO()
                {
                    Id = 2,
                    Age=35,
                    Gender = "Erkek",
                    Name = "Mahmut",
                    IdentityNumber = "12345678902",
                    LastName = "Sucuk",
                    LicenseType = "Yüksek"
                },
                new DriverDTO()
                {
                    Id = 3,
                    Age = 42,
                    Gender = "Erkek",
                    Name = "Ahmet",
                    IdentityNumber = "12345678903",
                    LastName = "Köfte",
                    LicenseType = "Standart"
                },
                new DriverDTO()
                {
                    Id = 4,
                    Age = 38,
                    Gender = "Erkek",
                    Name = "Yavuz",
                    IdentityNumber = "12345678904",
                    LastName = "Hırsız",
                    LicenseType = "Standart"
                }
            };
        }
    }
}
