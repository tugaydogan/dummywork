﻿using dummy.Entities;
using dummy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dummy.Data
{

    public class RouteMockData
    {
        public static RouteMockData Current { get; } = new RouteMockData();
        public List<RouteDTO> Routes { get; set; }

        public RouteMockData()
        {

            Routes = new List<RouteDTO>()
            {
                new RouteDTO()
                {
                    Id=1,
                    ArrivalLocation="Ankara",
                    DepartureLocation="İstanbul",
                    Distance= 400
                },
                 new RouteDTO()
                {
                    Id=2,
                    ArrivalLocation="Ordu",
                    DepartureLocation="İstanbul",
                    Distance= 900
                },
                  new RouteDTO()
                {
                    Id=3,
                    ArrivalLocation="Sivas",
                    DepartureLocation="Bolu",
                    Distance= 600
                }, 
                   new RouteDTO()
                {
                    Id=4,
                    ArrivalLocation="İzmir",
                    DepartureLocation="Çanakkale",
                    Distance= 350
                }
            };
        }
    }
}  

