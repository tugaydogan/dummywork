﻿using dummy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dummy.Data
{
    public class HostMockData
    {
        public static HostMockData Current { get; } = new HostMockData();

        public List<HostDTO> Hosts { get; set; }

        public HostMockData()
        {
            Hosts = new List<HostDTO>()
            {
                new HostDTO()
                {
                    Id = 1,
                    Name = "Tugay",
                    LastName="Doğan",
                    Age = 27,
                    Gender = "Erkek"
                },
                new HostDTO()
                {
                    Id = 2,
                    Name= "Melih",
                    LastName="Ergen",
                    Age = 27,
                    Gender="Erkek"
                },
                new HostDTO()
                {
                    Id=3,
                    Name="Cansın",
                    LastName="İmir",
                    Age= 30,
                    Gender="Erkek"
                }
            };
        }
    }
}
