﻿using dummy.Data;
using dummy.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dummy.Controllers
{
    [ApiController]
    public class DriverController:Controller
    {
        [HttpGet("api/drivers")]
        public IActionResult GetDrivers()
        {
            var temp = new JsonResult(DriverMockData.Current.Drivers);
            temp.StatusCode = 200;
            return Ok(temp);
        }
        [HttpGet("api/drivers/{id}")]
        public IActionResult GetDriverById(int id)
        {
            var driverReturn = new JsonResult(DriverMockData.Current.Drivers.FirstOrDefault(d => d.Id == id));
            if (driverReturn == null)
            {
                return NotFound();
            }
            return Ok(driverReturn);
        }
        [HttpPost("api/drivers")]
        public IActionResult CreateDriver([FromBody] DriverCreationDTO driver)
        {
            try
            {
                var date = Convert.ToDateTime(driver.Date).Date;
                var driverStore = DriverMockData.Current.Drivers;
                var maxId = driverStore.Max(d => d.Id);
                var newDriver = new DriverDTO()
                {
                    Id = maxId + 1,
                    Name = driver.Name,
                    LastName = driver.LastName,
                    Gender = driver.Gender,
                    Age = Int32.Parse(driver.Age),
                    LicenseType = driver.LicenseType,
                    IdentityNumber = driver.IdentityNumber,
                    Date = date
                };

                driverStore.Add(newDriver);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpPut("api/drivers/{driverId}")]
        public IActionResult UpdateDriver(int driverId,[FromBody] DriverUpdateDTO driver)
        {
            try
            {
                var driverFromStore = DriverMockData.Current.Drivers.FirstOrDefault(d => d.Id == driverId);
                if (driverFromStore == null)
                    return NotFound();
                driverFromStore.Name = driver.Name;
                driverFromStore.LastName = driver.LastName;
                driverFromStore.Gender = driver.Gender;
                driverFromStore.Age = Int32.Parse(driver.Age);
                driverFromStore.LicenseType = driver.LicenseType;
                driverFromStore.IdentityNumber = driver.IdentityNumber;
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpDelete("api/drivers/{driverId}")]
        public IActionResult DeleteDriver(int driverId)
        {
            try
            {
                var driverStore = DriverMockData.Current.Drivers;
                var driverFromStore = DriverMockData.Current.Drivers.FirstOrDefault(d => d.Id == driverId);
                if (driverFromStore == null)
                {
                    return NotFound();
                }
                else
                {
                    driverStore.Remove(driverFromStore);
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
