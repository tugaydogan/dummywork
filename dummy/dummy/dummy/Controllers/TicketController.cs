﻿using dummy.Data;
using dummy.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace dummy.Controllers
{
    [ApiController]
    [Route("api/expeditions")]
    public class TicketController : Controller
    {
        [HttpGet("{expeditionId}/tickets")]
        public IActionResult GetTickets(int expeditionId)
        {
            try
            {
                var expeditionStore = ExpeditionMockData.Current.Expeditions;
                var expedition = expeditionStore.FirstOrDefault(e => e.Id == expeditionId);
                var tickets = expedition.Tickets;
                return Ok(tickets);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [HttpGet("{expeditionId}/tickets/{ticketId}")]
        public IActionResult GetTicketById(int expeditionId, int ticketId)
        {
            try 
            {
                var expeditionStore = ExpeditionMockData.Current.Expeditions;
                var expedition = expeditionStore.FirstOrDefault(e => e.Id == expeditionId);
                var ticket = expedition.Tickets.FirstOrDefault(t => t.Id == ticketId);
                return Ok(ticket);
            } 
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        

        [HttpPut("{expeditionId}/tickets/sellTicket/{ticketId}")]
        public IActionResult TicketSale(int expeditionId, int ticketId)
        {
            try
            {
                var passenger = PassengerMockData.Current.Passengers.LastOrDefault();
                //PassengerDTO newPassenger = null;
                var expeditionStore = ExpeditionMockData.Current.Expeditions;
                var ticketStore = SoldTicketsMockData.Current.Tickets;
                var expedition = expeditionStore.FirstOrDefault(e => e.Id == expeditionId);
                var ticketFromStore = expedition.Tickets.FirstOrDefault(t => t.Id == ticketId);
                //try
                //{
                //    int maxId;
                //    var passengerStore = PassengerMockData.Current.Passengers;
                //    if (passengerStore.Count == 0)
                //        maxId = 0;
                //    else
                //        maxId = passengerStore.Max(p => p.Id);
                //    newPassenger = new PassengerDTO()
                //    {
                //        Id = maxId + 1,
                //        Name = passenger.Name,
                //        LastName = passenger.LastName,
                //        Age = passenger.Age,
                //        Gender = passenger.Gender
                //    };
                //}
                //catch(Exception ex)
                //{
                //    return BadRequest(ex.Message);
                //}
                

                if (ticketFromStore.isSold == false)
                {
                    ticketFromStore.isSold = true;
                    ticketFromStore.Passenger = passenger;
                    ticketStore.Add(ticketFromStore);
                }
                else
                    return BadRequest();
               

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [HttpPut("{expeditionId}/tickets/cancelTicket/{ticketId}")]
        public IActionResult CancelTicket(int expeditionId, int ticketId)
        {
            try
            {
                var expeditionStore = ExpeditionMockData.Current.Expeditions;
                var expedition = expeditionStore.FirstOrDefault(e => e.Id == expeditionId);
                var ticketFromStore = expedition.Tickets.FirstOrDefault(t => t.Id == ticketId);

                if (ticketFromStore.isSold == true)
                    ticketFromStore.isSold = false;
                else
                    return BadRequest();

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}