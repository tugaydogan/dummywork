﻿using System;
using System.Linq;
using dummy.Data;
using dummy.Models;
using Microsoft.AspNetCore.Mvc;

namespace dummy.Controllers
{
    [ApiController]
    public class HostController : Controller
    {
        [HttpGet("api/hosts")]
        public IActionResult GetHosts()
        {
            var temp = new JsonResult(HostMockData.Current.Hosts);
            temp.StatusCode = 200;
            return Ok(temp);
        }
        [HttpGet("api/hosts/{id}")]
        public IActionResult GetHostById(int id)
        {
            var hostReturn = new JsonResult(HostMockData.Current.Hosts.FirstOrDefault(h => h.Id == id));
            if(hostReturn == null)
            {
                return NotFound();
            }
            return Ok(hostReturn);
        }
        [HttpPost("api/hosts")]
        public IActionResult CreateHost([FromBody] HostCreationDTO host)
        {
        //    if(host == null)
        //    {
        //        return BadRequest();
        //    }
        //    else if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }
            try
            {
                var hostDate = Convert.ToDateTime(host.date).Date;
                var hostStore = HostMockData.Current.Hosts;
                var maxId = hostStore.Max(h => h.Id);
                var newHost = new HostDTO()
                {
                    Id = maxId + 1,
                    Name = host.Name,
                    LastName = host.LastName,
                    Age = Int32.Parse(host.Age),
                    Gender = host.Gender,
                    date = hostDate
                };
                HostMockData.Current.Hosts.Add(newHost);
                return Ok();

            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("api/hosts/{hostId}")]
        public IActionResult UpdateHost(int hostId, [FromBody] HostUpdateDTO host)
        {
            //if(host == null)
            //{
            //    return BadRequest();
                
            //}
            //else if (!ModelState.IsValid)
            //{
            //    return BadRequest(ModelState);
            //}
            
            try
            {
                var hostFromStore = HostMockData.Current.Hosts.FirstOrDefault(h => h.Id == hostId);
                if (hostFromStore == null)
                {
                    return NotFound();
                }
                hostFromStore.Name = host.Name;
                hostFromStore.LastName = host.LastName;
                hostFromStore.Gender = host.Gender;
                hostFromStore.Age = Int32.Parse(host.Age);
                return Ok();
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpDelete("api/hosts/{hostId}")]
        public IActionResult DeleteHost(int hostId)
        {
            try
            {
                var hostStore = HostMockData.Current.Hosts;
                var hostFromStore = hostStore.FirstOrDefault(h => h.Id == hostId);
                if(hostFromStore == null)
                {
                    return NotFound();
                }
                else
                {
                    hostStore.Remove(hostFromStore);
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
