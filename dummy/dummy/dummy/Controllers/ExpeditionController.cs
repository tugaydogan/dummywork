﻿using System;
using System.Collections.Generic;
using System.Linq;
using dummy.Data;
using dummy.Models;
using Microsoft.AspNetCore.Mvc;

namespace dummy.Controllers
{
    [ApiController]
    public class ExpeditionController : Controller
    {
        [HttpGet("api/expeditions/{dateTime}")]
        public IActionResult GetAllExpeditions(string dateTime)
        {
            try
            {
                var expData = ExpeditionMockData.Current.Expeditions;
                var date = Convert.ToDateTime(dateTime).Date;
                var getExps = expData.FindAll(e => e.DepartureDate == date);
                var temp = new JsonResult(getExps);
                temp.StatusCode = 200;
                return Ok(temp);
            }
            catch (Exception ex )
            {
                return BadRequest(ex.Message);
            }

        }      
        [HttpPost("api/expeditions")]
        public IActionResult saveNewExpedition(ExpeditionCreationDTO expedition)
        {
            try
            {
                int maxId;
                var departureDate = Convert.ToDateTime(expedition.DepartureDate);
                var arrivalDate = Convert.ToDateTime(expedition.ArrivalDate);
                var busStore = BusMockaData.Current.Buses;
                var routeStore = RouteMockData.Current.Routes;
                var driverStore = DriverMockData.Current.Drivers;
                var hostStore = HostMockData.Current.Hosts;
                var expeditionStore = ExpeditionMockData.Current.Expeditions;
                var Bus = busStore.FirstOrDefault(b => b.Id == Int32.Parse(expedition.BusId));
                var Driver = driverStore.FirstOrDefault(d => d.Id == Int32.Parse(expedition.DriverId));
                var Host = hostStore.FirstOrDefault(b => b.Id == Int32.Parse(expedition.HostId));
                var Route = routeStore.FirstOrDefault(b => b.Id == Int32.Parse(expedition.RouteId));
                if (expeditionStore.Count == 0)
                    maxId = 0;
                else
                    maxId = expeditionStore.Max(e => e.Id);

                var newExpedition = new ExpeditionDTO(Bus, Route, Host, Driver)
                {
                    Id = maxId + 1,
                    DepartureDate = departureDate,
                    ArrivalDate = arrivalDate
                };
                expeditionStore.Add(newExpedition);
                return Ok();
            }
            catch(Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}