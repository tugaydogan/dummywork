﻿using System;
using System.Linq;
using dummy.Data;
using dummy.Models;
using Microsoft.AspNetCore.Mvc;

namespace dummy.Controllers
{
    //[Route("api/routes")]
    [ApiController]
    public class RouteController : Controller
    {
        [HttpGet("api/routes")]
        public IActionResult GetRoutes()
        {
            //Status Code example
            var temp = new JsonResult(RouteMockData.Current.Routes);
            temp.StatusCode = 200;
            return Ok(temp);
        }
        [HttpGet("api/routes/{id}")]
        public IActionResult GetRouteById(int id)
        {
            var routeReturn = new JsonResult(RouteMockData.Current.Routes.FirstOrDefault(x => x.Id == id));
            //Status Code find example
            if (routeReturn == null)
            {
                return NotFound();
            }
            // result always JSON
            return Ok(routeReturn);
        }
        [HttpPost("api/routes")]
        public IActionResult CreateRoute([FromBody] RouteCreationDTO route)
        {
            /*
            if (route == null)
            {
                return BadRequest();
            }
            else if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            else if (route.ArrivalLocation == route.DepartureLocation)
            {
                ModelState.AddModelError("ArrivalLocation", "Must different from DepartureLocation");
            }
            */
            try
            {
                var routeStore = RouteMockData.Current.Routes;
                var maxId = routeStore.Max(r => r.Id);
                var newRoute = new RouteDTO()
                {
                    Id = maxId + 1,
                    DepartureLocation = route.DepartureLocation,
                    ArrivalLocation = route.ArrivalLocation,
                    Distance = Int32.Parse(route.Distance)
                };

                RouteMockData.Current.Routes.Add(newRoute);

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            
        }
        [HttpPut("api/routes/{routeId}")]
        public IActionResult UpdateRoute(int routeId,[FromBody] RouteUpdateDTO route)
        {
            /*if (route == null)
            {
                return BadRequest();
            }
            else if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            else if (route.ArrivalLocation == route.DepartureLocation)
            {
                ModelState.AddModelError("ArrivalLocation", "Must different from DepartureLocation");
            }
            
            */
            try
            {
                var routeFromStore = RouteMockData.Current.Routes.FirstOrDefault(x => x.Id == routeId);
                if (routeFromStore == null)
                {
                    return NotFound();
                }
                routeFromStore.ArrivalLocation = route.ArrivalLocation;
                routeFromStore.DepartureLocation = route.DepartureLocation;
                routeFromStore.Distance = Int32.Parse(route.Distance);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }
        [HttpDelete("api/routes/{routeId}")]
        public IActionResult DeleteRoute(int routeId) 
        {
            try
            {
                var routeStore = RouteMockData.Current.Routes;
                var routeFromStore = routeStore.FirstOrDefault(x => x.Id == routeId);
                if (routeFromStore == null)
                {
                    return NotFound();
                }
                else
                {
                    routeStore.Remove(routeFromStore);
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}