﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace dummy.Controllers
{
    [ApiController]
    public class TestController : Controller
    {
        [HttpPost("api/test")]
        public IActionResult Index()
        {
            return View();
        }
    }
}