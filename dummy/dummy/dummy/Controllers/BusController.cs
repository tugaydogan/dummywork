﻿using System;
using System.Linq;
using dummy.Data;
using dummy.Models;
using Microsoft.AspNetCore.Mvc;

namespace dummy.Controllers
{
    public class BusController : Controller
    {
        [HttpGet("api/buses")]
        public IActionResult GetBuses()
        {
            //Status Code example
            var temp = new JsonResult(BusMockaData.Current.Buses);
            temp.StatusCode = 200;
            return Ok(temp);
        }
        [HttpGet("api/buses/{id}")]
        public IActionResult GetBusById(int id)
        {
            var busReturn = new JsonResult(BusMockaData.Current.Buses.FirstOrDefault(x => x.Id == id));
            //Status Code find example
            if (busReturn == null)
            {
                return NotFound();
            }
            // result always JSON
            return Ok(busReturn);
        }
        [HttpPost("api/buses")]
        public IActionResult CreateBus([FromBody] BusCreationDTO bus)
        {
            /*
            if (route == null)
            {
                return BadRequest();
            }
            else if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            else if (route.ArrivalLocation == route.DepartureLocation)
            {
                ModelState.AddModelError("ArrivalLocation", "Must different from DepartureLocation");
            }
            */
            try
            {
                var busStore = BusMockaData.Current.Buses;
                var maxId = busStore.Max(r => r.Id);
                var newBus = new BusDTO()
                {
                    Id = maxId + 1,
                    Make = bus.Make,
                    Plate = bus.Plate,
                };
                busStore.Add(newBus);

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }
        [HttpPut("api/buses/{busId}")]
        public IActionResult UpdateBus(int busId, [FromBody] BusUpdateDTO bus)
        {
            /*if (route == null)
            {
                return BadRequest();
            }
            else if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            else if (route.ArrivalLocation == route.DepartureLocation)
            {
                ModelState.AddModelError("ArrivalLocation", "Must different from DepartureLocation");
            }
            
            */
            try
            {
                var busFromStore = BusMockaData.Current.Buses.FirstOrDefault(x => x.Id == busId);
                if (busFromStore == null)
                {
                    return NotFound();
                }
                busFromStore.Make = bus.Make;
                busFromStore.Plate = bus.Plate;
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }
        [HttpDelete("api/buses/{busId}")]
        public IActionResult DeleteBus(int busId)
        {
            try
            {
                var busStore = BusMockaData.Current.Buses;
                var busFromStore = busStore.FirstOrDefault(x => x.Id == busId);
                if (busFromStore == null)
                {
                    return NotFound();
                }
                else
                {
                    busStore.Remove(busFromStore);
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}