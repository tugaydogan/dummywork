﻿using System;
using System.Linq;
using dummy.Data;
using dummy.Models;
using Microsoft.AspNetCore.Mvc;

namespace dummy.Controllers
{
    [ApiController]
    public class PassengerController : Controller
    {
        [HttpGet("api/passengers")]
        public IActionResult GetPassenger()
        {
            try
            {
                var passengerStore = PassengerMockData.Current.Passengers;
                var temp = new JsonResult(passengerStore);
                return Ok(temp);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("api/passengers")]
        public IActionResult SavePassenger(PassengerCreationDTO passenger)
        {
            try
            {
                int maxId;
                var date = Convert.ToDateTime(passenger.Date).Date;
                var passengerStore = PassengerMockData.Current.Passengers;
                if (passengerStore.Count == 0)
                    maxId = 0;
                else
                    maxId = passengerStore.Max(p => p.Id);
                var newPassenger = new PassengerDTO()
                {
                    Id = maxId + 1,
                    Name = passenger.Name,
                    LastName = passenger.LastName,
                    Age = passenger.Age,
                    Gender = passenger.Gender,
                    Date = date
                };
                passengerStore.Add(newPassenger);
                return Ok();
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
