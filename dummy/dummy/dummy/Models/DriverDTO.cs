﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dummy.Models
{
    public class DriverDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public string Gender { get; set; }
        public string LicenseType { get; set; }
        public string IdentityNumber { get; set; }
        public string FullName { get { return Name + "-" + LastName; } }
        public DateTime? Date { get; set; }
    }
}
