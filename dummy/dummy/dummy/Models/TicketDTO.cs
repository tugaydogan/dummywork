﻿namespace dummy.Models
{
    public class TicketDTO
    {
        public int Id { get; set; }
        public int SeatNumber { get; set; }
        public bool isSold { get; set; }
        public string Side {
            get
            {
                if (SeatNumber % 3 == 1)
                {
                    return "left";
                }
                else if (SeatNumber % 3 == 2 )
                {
                    return "mid";
                }
                else
                {
                    return "right";
                }
            }
        }
        public PassengerDTO Passenger { get; set; }
        //public decimal PaidAmount { get; set; }
        //public string PassengerName { get; set; }
        //public string PassengerLastName { get; set; }
        //public string PassengerIdentityNumber { get; set; }
        //public PassengerDTO Passenger { get; set; }
        //public string ExpeditionCode { get; set; }
        //public ExpeditionDTO Expedition { get; set; }
    }
}
