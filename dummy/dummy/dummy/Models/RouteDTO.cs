﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dummy.Models
{
    public class RouteDTO
    {
        public int Id { get; set; }
        public string DepartureLocation { get; set; }
        public string ArrivalLocation { get; set; }
        public int Distance { get; set; }
        public string Name {
            get { return DepartureLocation + "-" + ArrivalLocation; }
        }
    }
}
