﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace dummy.Models
{
    public class DriverUpdateDTO
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Need a Name")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Need a Lastname")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Need a Age")]
        public string Age { get; set; }
        [Required(ErrorMessage = "Need a Gender")]
        public string Gender { get; set; }
        [Required(ErrorMessage = "Need a License Type")]
        public string LicenseType { get; set; }
        [Required(ErrorMessage = "Need a Identity Number")]
        public string IdentityNumber { get; set; }
    }
}
