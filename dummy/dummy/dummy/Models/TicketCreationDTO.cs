﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace dummy.Models
{
    public class TicketCreationDTO
    {
        public int Id { get; set; }
        public int SeatNumber { get; set; }
        public bool IsSold { get; set; }
        public string Side
        {
            get
            {
                if(SeatNumber % 3 == 1)
                {
                    return "left";
                }
                else if(SeatNumber % 3 == 2)
                {
                    return "mid";
                }
                else
                {
                    return "right";
                }
            }
        }
        public PassengerDTO Passenger { get; set; }
    }
}
