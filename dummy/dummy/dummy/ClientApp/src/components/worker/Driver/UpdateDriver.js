﻿import React, { useEffect, useState } from "react";
import * as actions from "../../../redux/actions/driverActions";
import { connect } from "react-redux";
import NavBar from "../../common/NavBar";
import Footer from "../../areas/Footer";
import Header from "../../areas/Header";

function UpdateHost({ history, ...props }) {
  const [driver, setDriver] = useState({ ...props.driver });
  const id = props.match.params.id;

  useEffect(() => {
    props.getByIdDriver(id);
  }, []);

  useEffect(() => {
    setDriver({ ...props.driver });
  }, [props.driver]);

  function handleChange(event) {
    const { name, value } = event.target;
    setDriver((prevHost) => ({
      ...prevHost,
      [name]: value,
    }));
  }

  function handleSave(event) {
    event.preventDefault();
    props.updateDriver(id, driver).then(() => history.push("/hosts"));
  }

  return (
    <div>
      <Header />
      <NavBar />

      <form onSubmit={handleSave} id="createhostformmargin">
        <div className="row">
          <div className="col">
            <label>İsim</label>
            <input
              name="name"
              type="text"
              onChange={handleChange}
              value={driver.name}
              className="form-control"
              placeholder="Emre"
            />
          </div>
          <div className="col">
            <label>Soyisim</label>
            <input
              name="lastName"
              type="text"
              onChange={handleChange}
              value={driver.lastName}
              className="form-control"
              placeholder="Yıldırım"
            />
          </div>
          <div className="col">
            <label>Yaş</label>
            <input
              name="age"
              type="text"
              onChange={handleChange}
              value={driver.age}
              className="form-control"
              placeholder="25"
            />
          </div>
          <div className="col">
            <label>DateTime</label>
            <input
              id="start"
              name="date"
              type="datetime-local"
              onChange={handleChange}
              value={driver.date}
              className="form-control"
            />
          </div>
          <div className="col">
            <label>Cinsiyet</label>
            <input
              name="gender"
              type="text"
              onChange={handleChange}
              value={driver.gender}
              className="form-control"
              placeholder="Erkek"
            />
          </div>
          <div className="col">
            <label>Ehliyet Türü</label>
            <input
              name="licenseType"
              type="text"
              onChange={handleChange}
              value={driver.licenseType}
              className="form-control"
            />
          </div>
        </div>
        <div id="hostexpeditionsmargin">
          <button type="submit" className="btn btn-success btn-lg">
            Save
          </button>
        </div>
      </form>
      <Footer />
    </div>
  );
}

/*UpdateHost.propTypes = {
    updateHost: PropTypes.func.isRequired,
    getByIdHost: PropTypes.func.isRequired,
    host: PropTypes.object.isRequired,
    handleChange: PropTypes.func.isRequired,
    handleSave: PropTypes.func.isRequired
};*/

const mapStateToProps = (state) => ({
  driver: state.drivers.capturedDriver,
  statusCode: state.drivers.statusCode,
  contentType: state.drivers.contentType,
});

const mapDispatchToProps = {
  updateDriver: actions.updateDriver,
  getByIdDriver: actions.getDriverById,
};

export default connect(mapStateToProps, mapDispatchToProps)(UpdateHost);
