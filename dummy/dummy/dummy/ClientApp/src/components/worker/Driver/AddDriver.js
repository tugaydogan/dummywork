﻿import React, { useEffect, useState } from "react";
import * as actions from "../../../redux/actions/driverActions";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import NavBar from "../../common/NavBar";
import Footer from "../../areas/Footer";
import Header from "../..//areas/Header";
import "../worker.css";

function AddDriver({ history, ...props }) {
    const [driver, setDriver] = useState({ ...props.driver });

    useEffect(() => {
        setDriver({ ...props.driver });
    }, []);

    function handleChange(event) {
        const { name, value } = event.target;
        setDriver((prevDriver) => ({
            ...prevDriver,
            [name]: name === "name" ? value : value,
            [name]: name === "lastName" ? value : value,
            [name]: name === "age" ? parseInt(value, 10) : value,
            [name]: name === "gender" ? value : value,
            [name]: name === "licanseType" ? value : value,
            [name]: name === "identityNumber" ? value : value,
            [name]: name === "date" ? value.toString() : value,
        }));
    }

    function handleSave(event) {
        event.preventDefault();
        props.addDriver(driver).then(() => {
            history.push("/drivers");
        });
    }

    return (
        <div className="container">
            <Header />
            <NavBar />
            <form onSubmit={handleSave} id="createrouteformmargin">
                <div className="row">
                    <div className="col">
                        <label>İsim</label>
                        <input
                            id="value1"
                            name="name"
                            type="text"
                            onChange={handleChange}
                            value={driver.name}
                            className="form-control"
                        />
                    </div>
                    <div className="col">
                        <label>Soyisim</label>
                        <input
                            id="value2"
                            name="lastName"
                            type="text"
                            onChange={handleChange}
                            value={driver.lastName}
                            className="form-control"
                        />
                    </div>
                    <div className="col">
                        <label>Yaş</label>
                        <input
                            name="age"
                            type="text"
                            onChange={handleChange}
                            value={driver.age}
                            className="form-control"
                        />
                    </div>
                    <div className="col">
                        <label>DateTime</label>
                        <input
                            id="start"
                            name="date"
                            type="datetime-local"
                            onChange={handleChange}
                            value={driver.date}
                            className="form-control"
                        />
                    </div>
                    <div className="col">
                        <label>Cinsiyet</label>
                        <input
                            name="gender"
                            type="text"
                            onChange={handleChange}
                            value={driver.gender}
                            className="form-control"
                        />
                    </div>
                    <div className="col">
                        <label>Ehliyet Türü</label>
                        <input
                            name="licenseType"
                            type="text"
                            onChange={handleChange}
                            value={driver.licenseType}
                            className="form-control"
                        />
                    </div>
                    <div className="col">
                        <label>Kimlik Numarası</label>
                        <input
                            name="identityNumber"
                            type="text"
                            onChange={handleChange}
                            value={driver.identityNumber}
                            className="form-control"
                        />
                    </div>
                </div>
                <div id="kaydetmebuttonu">
                    <button type="submit" className="btn btn-success btn-lg">
                        Save
          </button>
                </div>
            </form>
            <Footer />
        </div>
    );
}

AddDriver.propTypes = {
    addDriver: PropTypes.func.isRequired,
    driver: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
    driver: state.drivers.drivers,
});

const mapDispatchToProps = {
    addDriver: actions.saveNewDriver,
};

export default connect(mapStateToProps, mapDispatchToProps)(AddDriver);
