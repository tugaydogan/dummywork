﻿import React, { useEffect, useState } from "react";
import * as actions from "../../redux/actions/routeActions";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import NavBar from "../common/NavBar";
import Footer from "../areas/Footer";
import Header from "../areas/Header";

function AddRoute({ history, ...props }) {
  const [route, setRoute] = useState({ ...props.route });

  useEffect(() => {
    setRoute({ ...props.route });
  }, [props.route]);

  function handleChange(event) {
    const { name, value } = event.target;
    setRoute((prevRoute) => ({
      ...prevRoute,
      [name]: name === "distance" ? parseInt(value, 10) : value,
      [name]: name === "arrivalLocation" ? value : value,
      [name]: name === "departureLocation" ? value : value,
    }));
  }

  function handleSave(event) {
    event.preventDefault();
    props.addRoute(route).then(() => {
      history.push("/routes");
    });
  }

    return (
        <div className="container">
            <Header />
            <NavBar />
            <form onSubmit={handleSave} id="createrouteformmargin">
                <div className="row">
                    <div className="col">
                        <label>Kalkış Yeri</label>
                        <input
                            id="value1"
                            name="departureLocation"
                            type="text"
                            onChange={handleChange}
                            value={route.departureLocation}
                            className="form-control"
                        //placeholder="İstanbul"
                        />
                    </div>
                    <div className="col">
                        <label>Varış Yeri</label>
                        <input
                            id="value2"
                            name="arrivalLocation"
                            type="text"
                            onChange={handleChange}
                            value={route.arrivalLocation}
                            className="form-control"
                        //placeholder="Ankara"
                        />
                    </div>
                    <div className="col">
                        <label>Mesafe</label>
                        <input
                            id="value3"
                            name="distance"
                            type="text"
                            onChange={handleChange}
                            value={route.distance}
                            className="form-control"
                        //placeholder="450km"
                        />
                    </div>
                </div>
                <div id="routeexpeditionsmargin">
                    <button type="submit" className="btn btn-success btn-lg">
                        Save
          </button>
                </div>
            </form>
            <Footer />
        </div>
    );
}

AddRoute.propTypes = {
  addRoute: PropTypes.func.isRequired,
  route: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  route: state.routes.newRoute,
});

const mapDispatchToProps = {
  addRoute: actions.saveNewRoute,
};

export default connect(mapStateToProps, mapDispatchToProps)(AddRoute);
