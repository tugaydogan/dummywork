﻿import React, { useEffect, useState } from 'react';
import NavBar from "../common/NavBar";
import Header from "../areas/Header";
import Footer from "../areas/Footer";
import { connect } from 'react-redux';
import * as ticketActions from "../../redux/actions/ticketActions";
import "../bus/Bus.css";
import * as passengerActions from '../../redux/actions/passengerActions';

function PassengerInfo({ history, ...props }) {

    const [passenger, setPassenger] = useState({ ...props.passenger })
    const expeditionId = props.match.params.expId;
    const capturetTicketId = props.match.params.id;

    useEffect(() => {
        setPassenger({ ...props.passenger });
    }, [props.passenger]);

    function handleChange(event) {
        const { name, value } = event.target;
        setPassenger((prevPassenger) => ({
            ...prevPassenger,
            [name]: name === "name" ? value : value,
            [name]: name === "lastName" ? value : value,
            [name]: name === "age" ? parseInt(value, 10) : value,
        }));
    }

    function handleSave(event) {
        debugger;
        event.preventDefault();
        props.sellTicket(expeditionId, capturetTicketId);
        props.createPassenger(expeditionId, capturetTicketId, passenger);
    }

    return (
        <div className="container">
            <p className="blog-header-logo text-dark">Yolcu Bilgilerinizi Giriniz</p>
            <form submit={handleSave}>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="input">İsim</label>
                        <input
                            name="name"
                            type="text"
                            onChange={handleChange}
                            value={passenger.name}
                            className="form-control"
                            placeholder="Tugay"
                        />
                    </div>
                    <div class="form-group col-md-6">
                        <label for="input">Soyisim</label>
                        <input
                            name="lastName"
                            type="text"
                            onChange={handleChange}
                            value={passenger.lastname}
                            className="form-control"
                            placeholder="Doğan"
                        />
                    </div>
                </div>
                <div class="form-group">
                    <label for="input">Yaş</label>
                    <input
                        name="age"
                        type="text"
                        onChange={handleChange}
                        value={passenger.age}
                        className="form-control"
                        placeholder="27"
                    />
                </div>
                <label id="mahmut">Cinsiyet</label>
                <div className="radio">
                    <input
                        type="radio"
                        name="gender"
                        onChange={handleChange}
                        value="Kadın"
                        checked={passenger.gender === "Kadın"}
                    />
                    <label class="form-check-label" for="exampleRadios1">
                        Kadın
            </label>
                </div>
                <div class="radio">
                    <input
                        type="radio"
                        name="gender"
                        value="Erkek"
                        checked={passenger.gender === "Erkek"}
                        onChange={handleChange}
                    />
                    <label class="form-check-label" for="exampleRadios2">
                        Erkek
            </label>
                </div>
                <button type="submit" className="btn btn-success btn-lg">
                    Sat
        </button>
            </form>
        </div>
    );
}
const mapStateToProps = (state) => ({
    passenger: state.passengers.newPassenger
});

const mapDispatchToProps = {
    sellTicket: ticketActions.sellTicket,
    createPassenger: passengerActions.saveNewPassenger,
};

export default connect(mapStateToProps, mapDispatchToProps)(PassengerInfo);
//<div class="form-group">
//    <label for="inputAddress"></label>
//    <input
//        type="text"
//        class="form-control"
//        placeholder="12345678965"
//        onkeypress="return event.charCode>= 48 &&event.charCode<= 57"
//        maxLength="11"
//    />
//</div>