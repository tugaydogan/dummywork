﻿import React, { useState } from "react";

function Main() {
    const [startDate, setStartDate] = useState(new Date());

    function handleChange(event) {
        const { name, value } = event.target;
        setStartDate((prevRoute) => ({
            ...prevRoute,
            [name]: value,
        }));
    }

    function handleClick() {
        console.log(startDate);
    }
    return (
        <div className="container">
            <input type="date" id="start" min="2020-06-19" onChange={handleChange} />
            <button
                onClick={handleClick}
                type="submit"
                className="btn btn-success btn-lg"
            >
                BunA Bİ BAS
      </button>
        </div>
    );
}

export default Main;