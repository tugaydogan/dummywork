﻿import React, { useState } from "react";
import DatePicker from "react-datepicker";

function Main() {
    const [startDate, setStartDate] = useState(new Date());

    //function handleChange(event) {
    //    const { name, value } = event.target;
    //    setStartDate((prevRoute) => ({
    //        ...prevRoute,
    //        [name]: value,
    //    }));
    //}

    function handleClick() {
        console.log(startDate);
    }

    return (
        <div class="container">
            <div>
                <DatePicker
                    selected={startDate}
                    onChange={(date) => setStartDate(date)}
                />
            </div>
            <div>
                <button
                    onClick={handleClick}
                    type="submit"
                    className="btn btn-success btn-lg"
                >
                    BunA Bİ BAS
                </button>
            </div>

        </div>
    );
}

export default Main;
